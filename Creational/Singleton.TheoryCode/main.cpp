#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "Start of main..." << endl;

    Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();

    cout << "End of main..." << endl;
}
