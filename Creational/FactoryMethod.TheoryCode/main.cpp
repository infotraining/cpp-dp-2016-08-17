#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{       
        unique_ptr<Service> service = creator_->create_service(); // delegation to creator
        unique_ptr<Service> srv_moved = move(service);

        string result = srv_moved->run();
        cout << "Client is using: " << result << endl;
	}
};

int main()
{
    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("ServiceA", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("ServiceB", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("ServiceC", make_shared<ConcreteCreatorC>()));

    Client client(creators["ServiceC"]);
	client.use();
}
