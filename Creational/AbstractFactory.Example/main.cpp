#include "game.hpp"

using namespace Game;

int main()
{
	GameApp game;

    game.select_level(GameLevel::easy);
	game.init_game(12);
	game.test_monsters();
}
