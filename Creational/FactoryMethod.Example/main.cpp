#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>
#include <memory>
#include <functional>

#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "shape_factory.hpp"
#include "application.hpp"

using namespace Drawing;
using namespace std;

namespace Step1
{
    class ShapeCreator
    {
    public:
        virtual Shape* create_shape() const = 0;
        virtual ~ShapeCreator() = default;
    };


    class ShapeFactory
    {
        using CreatorPtr = std::unique_ptr<ShapeCreator>;
        std::map<std::string, CreatorPtr> creators_;
    public:
        bool register_creator(const std::string& id, CreatorPtr creator)
        {
            return creators_.insert(std::make_pair(id, move(creator))).second;
        }

        Shape* create_shape(const std::string& id)
        {
            auto& creator = creators_.at(id);
            return creator->create_shape();
        }
    };

}

namespace Proposition2
{
    using CreatorType = std::function<Shape*()>;


    class ShapeFactory
    {
        std::map<std::string, CreatorType> creators_;
    public:
        bool register_creator(const std::string& id, CreatorType creator)
        {
            return creators_.insert(std::make_pair(id, move(creator))).second;
        }

        Shape* create_shape(const std::string& id)
        {
            auto& creator = creators_.at(id);
            return creator();
        }
    };

}

//Shape* create_shape(const std::string& id)
//{
//	if (id == "Circle")
//		return new Circle();
//	else if (id == "Rectangle")
//		return new Rectangle();

//	throw std::runtime_error("Bad identifier");
//}

//using namespace Proposition2;

class GraphicsDocument : public Document
{
	vector<Shape*> shapes_;
	string title_;
    ShapeFactoryType& factory_;
public:
    GraphicsDocument(ShapeFactoryType& factory) : factory_(factory)
    {}

	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";

                Shape* shp_ptr = factory_.create_object(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());

        for(const auto& shape : shapes_)
            shape->write(fout);

		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";

        for(const auto& shape : shapes_)
            shape->draw();
	}

	~GraphicsDocument()
	{
		for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
			delete *it;
	}
};

class GraphicsApplication : public Application
{
    ShapeFactoryType& shape_creator_;
public:
    GraphicsApplication(ShapeFactoryType& shape_creator) : shape_creator_(shape_creator)
    {}

	virtual Document* create_document()
	{
        return new GraphicsDocument(shape_creator_);
	}
};


namespace Step1
{
    class CircleCreator : public ShapeCreator
    {
        // ShapeCreator interface
    public:
        Shape* create_shape() const
        {
            return new Circle();
        }
    };

    class RectangleCreator : public ShapeCreator
    {
        // ShapeCreator interface
    public:
        Shape* create_shape() const
        {
            return new Rectangle();
        }
    };
}



void foo(int x)
{
    std::cout << "foo(" << x << ")" << std::endl;
}

class Foo
{
public:
    void operator()(int x)
    {
        std::cout << "foo(" << x << ")" << std::endl;
    }
};

void function_explained()
{
    void (*ptr_fun)(int) = foo;
    ptr_fun(10); // call with ()

    Foo foonctor;
    foonctor(10); // call with ()

    auto lambda = [](int x) { std::cout << "lambda(" << x << ")" << std::endl;  };
    lambda(10);

    function<void(int)> generic_foo;

    generic_foo = ptr_fun;
    generic_foo(20);

    generic_foo = foonctor;
    generic_foo(30);

    generic_foo = lambda;
    generic_foo(40);
}

Rectangle* create_rect()
{
    return new Rectangle;
}

int main()
{
    //function_explained();

    GraphicsApplication app(ShapeFactory::instance());

    app.open_document("../drawing.txt");

    Document* ptr_doc = app.find_document("../drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
