#include "circle.hpp"
#include "clone_factory.hpp"

namespace
{
	bool is_registered = Drawing::ShapeFactory::instance().register_shape("Circle", new Drawing::Circle());
    bool is_registered_xl = Drawing::ShapeFactory::instance().register_shape("CircleXL", new Drawing::Circle(0, 0, 100));
}
