#include "shape_group.hpp"

namespace
{
    bool is_registered = Drawing::ShapeFactory::instance()
                               .register_shape("ShapeGroup", new Drawing::ShapeGroup());
}
