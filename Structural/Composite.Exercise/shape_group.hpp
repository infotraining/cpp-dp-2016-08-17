#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <memory>

namespace Drawing
{

    using ShapePtr = std::shared_ptr<Shape>;

// TO DO: zaimplementowac kompozyt grupujacy ksztalty geometryczne
class ShapeGroup : public Shape
{
    std::vector<ShapePtr> shapes_;

    // Shape interface
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& s : source.shapes_)
            shapes_.emplace_back(s->clone());
    }

    Shape& operator=(const ShapeGroup& source)
    {
        ShapeGroup temp(source);
        swap(temp);

        return *this;
    }

    void swap(ShapeGroup& other)
    {
        shapes_.swap(other.shapes_);
    }

    void draw() const override
    {
        for(const auto& s : shapes_)
            s->draw();
    }

    void move(int dx, int dy) override
    {
        for(const auto& s : shapes_)
            s->move(dx, dy);
    }

    void read(std::istream& in) override
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string id;
            in >> id;
            Shape* shp = ShapeFactory::instance().create(id);
            shp->read(in);
            shapes_.emplace_back(shp);
        }
    }

    void write(std::ostream& out) override
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& s : shapes_)
        {
            s->write(out);
        }
    }

    Shape*clone() const override
    {
        return new ShapeGroup(*this);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
