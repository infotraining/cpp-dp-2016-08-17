#include "line.hpp"
#include "clone_factory.hpp"

namespace
{
	bool is_registered = Drawing::ShapeFactory::instance().register_shape("Line", new Drawing::Line());
}