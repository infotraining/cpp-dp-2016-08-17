#include <iostream>
#include <string>

using namespace std;

struct Car
{
    string engine;
    string gearbox;
    string wheels[4];
    string aircondition;

    void info()
    {
        cout << "Car: " << engine << "; " << gearbox << "; " << aircondition << "; ";
        for(const auto& w : wheels)
            cout << w << " ";
        cout << "\n";
    }
};

class CarBuilder
{
protected:
    Car c;
public:
    Car construct() // template method
    {
        build_engine();
        build_gearbox();
        build_aircondition();
        build_wheels();

        return c;
    }

	virtual ~CarBuilder() = default;

protected:
    virtual void build_engine()
    {
        c.engine = "1.1L Petrol";
    }

    virtual void build_gearbox()
    {
        c.gearbox = "Manual 5";
    }

    virtual void build_aircondition()
    {}

    virtual void build_wheels()
    {
        for(auto& w : c.wheels)
            w = "16'";
    }
};

class PremiumCarBuilder : public CarBuilder
{
protected:
    void build_engine() override
    {
        c.engine = "3.5 Diesel";
    }

    void build_aircondition() override
    {
        c.aircondition = "full auto";
    }
};

int main(int argc, char *argv[])
{
    CarBuilder cb;

    auto car = cb.construct();
    car.info();
}
