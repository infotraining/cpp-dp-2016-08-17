#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>

using namespace std;

//bool is_greater_than_100(int x)
//{
//    return x > 100;
//}

class MagicLambda_23674523846528
{
   const int threshold_;
   int& position_;
public:
    MagicLambda_23674523846528(int threshold, int& position) : threshold_{threshold}, position_{position}
    {}

    bool operator()(int x) const { ++position_; return x > threshold_; }
};

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_it, max_it;
    tie(min_it, max_it) = minmax_element(data.begin(), data.end());

    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_it, *max_it, avg);
}


int main(int argc, char *argv[])
{
    vector<int> vec = { 1, 2, 3, 4, 5, 345, 543, 23446 };

    int threshold = 100;
    int position = 0;

    //auto it = find_if(vec.begin(), vec.end(), &is_greater_than_100);
    auto it = find_if(vec.begin(), vec.end(),
                      [threshold, &position](auto x) { ++position; return x > threshold; });

    if (it != vec.end())
    {
        cout << "I found: " << *it << " at " << position << endl;
    }

    int min, max;
    //double avg;

    tie(min, max, ignore) = calc_stats(vec);

    cout << "min: " << min << endl;
    cout << "max: " << max << endl;
    //cout << "avg: " << avg << endl;
}
